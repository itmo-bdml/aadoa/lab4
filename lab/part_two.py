import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import random
import pandas as pd
from math import sin, cos, sqrt, atan2
matplotlib.use('TkAgg')

random.seed(41)
np.random.seed(41)

cities = pd.read_csv('cities.csv')
print(cities)

initial_path = cities.loc[1:, ("id")].values.tolist()
random.shuffle(initial_path)
initial_path.insert(0, 1)
initial_path.append(1)


def calc_distance(lat1, lon1, lat2, lon2):
    # calc distance on a Sphere
    R = 6373.0
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (sin(dlat/2))**2 + cos(lat1) * cos(lat2) * (sin(dlon/2))**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = R * c
    return distance


distances_map = [[0] * len(cities) for i in range(len(cities))]
for i in range(len(cities)):
    for j in range(len(cities)):
        lat1 = cities.lat[i]
        lat2 = cities.lat[j]
        lon1 = cities.lon[i]
        lon2 = cities.lon[j]
        distances_map[i][j] = sqrt((lat2 - lat1)**2 + (lon2-lon1)**2)
        #map[i][j] = calc_distance(lon1, lon2, lat1, lat2)


def calc_new_distance(path):
    p = 0
    for i in range(len(path)-1):
        p += distances_map[path[i]-1][path[i+1]-1]
    return p


dist = calc_new_distance(initial_path)
T = 100000
# S_list = []
# S_list.append(S)
iters = 0
distance_history = [dist]
final_path = initial_path.copy()
while T > 1e-20 or iters < 1e-4:
    iters += 1
    to_swap = random.sample(range(15), 2)

    new_path = final_path.copy()
    temp_elem = new_path[to_swap[0]]
    new_path[to_swap[0]] = new_path[to_swap[1]]
    new_path[to_swap[1]] = temp_elem
    new_distance = calc_new_distance(new_path)
    if new_distance < distance_history[-1]:
        final_path = new_path
        distance_history.append(new_distance)
    else:
        d = new_distance - distance_history[-1]
        p = np.exp(-d / T)
        if random.uniform(0, 1) < p:
            final_path = new_path
            distance_history.append(new_distance)
    if not iters % 10000:
        print(iters, new_distance, T)
    T *= 0.995


plt.figure(figsize=(12, 8))
plt.subplot(121)
plt.scatter(x=cities.loc[:, ('lon')], y=cities.loc[:, 'lat'])
plt.grid()
plt.title("Initial state of the path")
for i in range(0, len(initial_path)-1):
    x0 = cities.loc[initial_path[i]-1, ('lon')]
    y0 = cities.loc[initial_path[i]-1, ('lat')]
    dx = cities.loc[initial_path[i+1]-1, ('lon')] - x0
    dy = cities.loc[initial_path[i+1]-1, ('lat')] - y0
    plt.arrow(x0, y0, dx, dy)

plt.subplot(122)
plt.scatter(x=cities.loc[:, ('lon')], y=cities.loc[:, 'lat'])
plt.grid()
plt.title("Final state of the path")
for i in range(0, len(initial_path)-1):
    x0 = cities.loc[final_path[i]-1, ('lon')]
    y0 = cities.loc[final_path[i]-1, ('lat')]
    dx = cities.loc[final_path[i+1]-1, ('lon')] - x0
    dy = cities.loc[final_path[i+1]-1, ('lat')] - y0
    plt.arrow(x0, y0, dx, dy)

x0 = cities.loc[final_path[-1]-1, ('lon')]
y0 = cities.loc[final_path[-1]-1, ('lat')]
dx = cities.loc[final_path[0]-1, ('lon')] - x0
dy = cities.loc[final_path[0]-1, ('lat')] - y0
plt.arrow(x0, y0, dx, dy)
plt.savefig("./plots/task_two.png")

plt.figure(figsize=(12, 8))
plt.grid()
plt.plot(distance_history)
plt.show()
plt.savefig("./plots/task_two_distance.png")
