import numpy as np
from scipy.optimize import minimize, curve_fit, dual_annealing, differential_evolution, least_squares
import matplotlib
import matplotlib.pyplot as plt
import pyswarm as ps
matplotlib.use('TkAgg')


np.random.seed(42)


def calc_f(x_: np.ndarray) -> np.ndarray:
    return np.array(1 / (x_**2 - 3*x_ + 2))


def generate_y(x_: np.ndarray, delta: np.ndarray) -> np.ndarray:
    y_ = calc_f(x_)
    y_ = np.where(y_ > 100, 100, y_)
    y_ = np.where(y_ < -100, -100, y_)
    y_ += delta
    return y_


delta = np.random.normal(scale=1, size=1001)
x = np.arange(0, 1001, 1) * 3 / 1000
y = generate_y(x, delta)


def func(x, a, b, c, d) -> float:
    return (a * x + b) / (x * x + c * x + d)


def mse_loss(coefs) -> float:
    a, b, c, d = coefs
    preds = (a*x + b) / (x*x + c*x + d)
    return np.sum((preds - y) ** 2)


def lm_func(coefs, x, y) -> float:
    y_pred = func(x, *coefs)
    return y_pred - y


# Nelder-Mead algorithm
nelder = minimize(mse_loss, [0., 0., 0., 0.1], method='Nelder-Mead', tol=1e-3,
                  options={'disp': False, 'xatol': 1e-3, 'fatol': 1e-3})
print("Nelder-Mead: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(nelder.x))
print("Opertaions: %.d, Iterations: %.d, fun: %.2f" % (nelder.nfev, nelder.nit, nelder.fun), end="\n\n")


lw = [-5] * 4
up = [5] * 4

# Levenberg-Marquardt algorithm
lma = curve_fit(func, x, y, [0, 0, -1, 1], full_output=True, method='lm', maxfev=1000000)
print("LMA: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(lma[0]), )
print("Opertaions: %.d, Iterations: %.d, fun: %.2f" % (lma[2]['nfev'], -1., mse_loss(lma[0])), end="\n\n")
start_values = [1., 1., -1., 1.]

#LMA2

# lma = least_squares(lm_func, start_values, method="lm", args=(x, y),  ftol=1e-3)
# print("LMA: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(lma.x), )
# print(lma)
# print("Opertaions: %.d, Iterations: %.d, , fun: %.2f" % (lma.nfev, -1, mse_loss(lma.x)), end="\n\n")

# Simulated Annealing
ae = dual_annealing(mse_loss, bounds=list(zip(lw, up)))
print("AE: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(ae.x), )
print("Opertaions: %.d, Iterations: %.d, , fun: %.2f" % (ae.nfev, ae.nit, ae.fun), end="\n\n")


# Differential Evolution
de = differential_evolution(mse_loss, bounds=list(zip(lw, up)), tol=1e-3)
print("Differential Evolution: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(de.x), )
print("Opertaions: %.d, Iterations: %.d, fun: %.2f" % (de.nfev, de.nit, de.fun), end="\n\n")


# tried PySwarms but it didn't work
# Particle Swarm
# To count the number of iterations set the debug = True
xopt, fopt = ps.pso(mse_loss, lw, up, swarmsize=100, maxiter=500, minfunc=1e-3, debug=False)
print("Patricle Swarm: f(x) = (%.2fx + %.2f) / (x^2 + %.2fx + %.2f)" % tuple(xopt), )
print("Opertaions: %.d, Iterations: %.d, , fun: %.2f" % (32500, 325, fopt), end="\n\n")


plt.figure(figsize=(12, 8))
plt.grid()
plt.scatter(x, y, zorder=1, label="\"observed\" data", s=24.)
plt.plot(x, (nelder.x[0]*x + nelder.x[1]) / (x*x + nelder.x[2]*x + nelder.x[3]), lw=2., zorder=2, c="orange", label="Nelder-Mead")
# plt.plot(x, (lma.x[0]*x + lma.x[1]) / (x*x + lma.x[2]*x + lma.x[3]), lw=2., zorder=3, c="red", label="LMA")
plt.plot(x, (lma[0][0]*x + lma[0][1]) / (x*x + lma[0][2]*x + lma[0][3]), lw=2., zorder=3, c="red", label="LMA")
plt.plot(x, (de.x[0]*x + de.x[1]) / (x*x + de.x[2]*x + de.x[3]), lw=2., zorder=4, c="purple", label="Differential Evolution")
plt.plot(x, (ae.x[0]*x + ae.x[1]) / (x*x + ae.x[2]*x + ae.x[3]), lw=1.5, zorder=5, c="yellow", label="Annealing", ls=':')
plt.plot(x, (xopt[0]*x + xopt[1]) / (x*x + xopt[2]*x + xopt[3]), lw=1.5, zorder=6, c="green", label="Swarm", ls='--')

plt.legend(loc="best")
plt.savefig("./plots/task_one.png")
plt.show()
